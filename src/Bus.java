public interface Bus {
    void subscribe(BusSubscriber busSubscriber, String messageType);
    void send(Message message);
}
