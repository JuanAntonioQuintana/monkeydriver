import java.time.LocalDateTime;

import static java.time.temporal.ChronoUnit.SECONDS;

public class AheadVehicleSpeed implements BusSubscriber, BusPublisher{

    private Bus bus;
    private AheadDistanceMessage initialDistance;
    private float speedOfVehicle = 0;

    @Override
    public void receive(Message message) {
        if (isSpeedMessage(message)){
            speedOfVehicle = ((SpeedMessage) message).speed();
        }
        if (isAheadDistanceMessage(message)){
            if (initialDistance == null){
                initialDistance = (AheadDistanceMessage) message;
            }
            try {
                sendMessage(CalculateSpeedWith((AheadDistanceMessage) message));
            } catch (Exception ignored){}
            initialDistance = (AheadDistanceMessage) message;
        }
    }

    private boolean isAheadDistanceMessage(Message message) {
        return message.type().equals("AheadDistance");
    }

    private boolean isSpeedMessage(Message message) {
        return message.type().equals("Speed");
    }

    private float CalculateSpeedWith(AheadDistanceMessage finalDistance) {
        return speedOfVehicle +
                (float) (differenceOfDistance(finalDistance) / differenceOfTime(finalDistance));
    }

    private int differenceOfDistance(AheadDistanceMessage finalDistance) {
        return finalDistance.distance() - initialDistance.distance();
    }

    private long differenceOfTime(AheadDistanceMessage finalDistance) {
        return SECONDS.between(initialDistance.time(), finalDistance.time());
    }

    @Override
    public void setBus(Bus bus) {
        this.bus = bus;
    }

    private void sendMessage(float speed) {
        bus.send(new AheadVehicleSpeedMessage() {
            @Override
            public float speed() {
                return speed;
            }

            @Override
            public String type() {
                return "AheadVehicleSpeed";
            }

            @Override
            public LocalDateTime time() {
                return null;
            }
        });
    }
}
