import java.time.LocalDateTime;

public interface Message {
    String type();
    LocalDateTime time();
}
