public interface SpeedMessage extends Message{
    float speed();
}
