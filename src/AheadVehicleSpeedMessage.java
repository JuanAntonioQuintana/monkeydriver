public interface AheadVehicleSpeedMessage extends Message{
    float speed();
}
