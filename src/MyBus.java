import java.util.ArrayList;
import java.util.List;

class MyBus implements Bus {

    private List<BusSubscriber> subscribers = new ArrayList<>();

    @Override
    public void subscribe(BusSubscriber busSubscriber, String messageType) {
        subscribers.add(busSubscriber);
    }

    @Override
    public void send(Message message) {
        subscribers.forEach(busSubscriber -> busSubscriber.receive(message));
    }
}
