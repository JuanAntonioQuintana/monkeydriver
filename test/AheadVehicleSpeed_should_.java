import org.junit.Test;
import org.mockito.ArgumentCaptor;

import java.time.LocalDateTime;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class AheadVehicleSpeed_should_ {

    private LocalDateTime initialTime = LocalDateTime.of(2016, 12, 20, 8, 0, 0);
    private LocalDateTime finalTime = LocalDateTime.of(2016, 12, 20, 8, 0, 2);

    private final SpeedMessage twentySpeedMessage = new SpeedMessage() {
        @Override
        public float speed() {
            return 20;
        }

        @Override
        public String type() {
            return "Speed";
        }

        @Override
        public LocalDateTime time() {
            return initialTime;
        }
    };
    private final SpeedMessage fortySpeedMessage = new SpeedMessage() {
        @Override
        public float speed() {
            return 40;
        }

        @Override
        public String type() {
            return "Speed";
        }

        @Override
        public LocalDateTime time() {
            return initialTime;
        }
    };
    private final PlateMessage plateMessage = new PlateMessage() {
        @Override
        public String plate() {
            return "1234AB";
        }

        @Override
        public String type() {
            return "Plate";
        }

        @Override
        public LocalDateTime time() {
            return initialTime;
        }
    };
    private final AheadDistanceMessage aheadDistanceVehicleAt30MessageInitial = new AheadDistanceMessage() {
        @Override
        public int distance() {
            return 30;
        }

        @Override
        public String type() {
            return "AheadDistance";
        }

        @Override
        public LocalDateTime time() {
            return initialTime;
        }
    };

    private final AheadDistanceMessage aheadDistanceVehicleAt30MessageFinal = new AheadDistanceMessage() {
        @Override
        public int distance() {
            return 30;
        }

        @Override
        public String type() {
            return "AheadDistance";
        }

        @Override
        public LocalDateTime time() {
            return finalTime;
        }
    };
    private AheadDistanceMessage aheadVehicleAt60 = new AheadDistanceMessage() {
        @Override
        public int distance() {
            return 60;
        }

        @Override
        public String type() {
            return "AheadDistance";
        }

        @Override
        public LocalDateTime time() {
            return finalTime;
        }
    };

    @Test
    public void send_a_AheadVehicleSpeedMessage_to_bus_with_the_speed_of_the_ahead_vehicle() {
        Bus bus = mock(Bus.class);
        AheadVehicleSpeed aheadVehicleSpeed = new AheadVehicleSpeed();
        aheadVehicleSpeed.setBus(bus);

        aheadVehicleSpeed.receive(twentySpeedMessage);
        aheadVehicleSpeed.receive(aheadDistanceVehicleAt30MessageInitial);
        aheadVehicleSpeed.receive(plateMessage);
        aheadVehicleSpeed.receive(twentySpeedMessage);
        aheadVehicleSpeed.receive(aheadDistanceVehicleAt30MessageFinal);
        aheadVehicleSpeed.receive(plateMessage);

        ArgumentCaptor<Message> captor = ArgumentCaptor.forClass(Message.class);
        verify(bus).send(captor.capture());
        AheadVehicleSpeedMessage aheadVehicleSpeedMessage = (AheadVehicleSpeedMessage) captor.getValue();
        assertThat(aheadVehicleSpeedMessage.type(), is("AheadVehicleSpeed"));
        assertThat(aheadVehicleSpeedMessage.speed(), is(20f));
    }

    @Test
    public void send_a_AheadVehicleSpeedMessage_to_bus_with_the_speed_of_40() {
        Bus bus = mock(Bus.class);
        AheadVehicleSpeed aheadVehicleSpeed = new AheadVehicleSpeed();
        aheadVehicleSpeed.setBus(bus);

        aheadVehicleSpeed.receive(fortySpeedMessage);
        aheadVehicleSpeed.receive(aheadDistanceVehicleAt30MessageInitial);
        aheadVehicleSpeed.receive(plateMessage);

        aheadVehicleSpeed.receive(fortySpeedMessage);
        aheadVehicleSpeed.receive(aheadDistanceVehicleAt30MessageFinal);
        aheadVehicleSpeed.receive(plateMessage);

        ArgumentCaptor<Message> captor = ArgumentCaptor.forClass(Message.class);
        verify(bus).send(captor.capture());
        AheadVehicleSpeedMessage aheadVehicleSpeedMessage = (AheadVehicleSpeedMessage) captor.getValue();
        assertThat(aheadVehicleSpeedMessage.type(), is("AheadVehicleSpeed"));
        assertThat(aheadVehicleSpeedMessage.speed(), is(40f));
    }

    @Test
    public void send_a_message_with_the_speed_of_the_ahead_vehicle() {
        Bus bus = mock(Bus.class);
        AheadVehicleSpeed aheadVehicleSpeed = new AheadVehicleSpeed();
        aheadVehicleSpeed.setBus(bus);

        aheadVehicleSpeed.receive(fortySpeedMessage);
        aheadVehicleSpeed.receive(aheadDistanceVehicleAt30MessageInitial);

        aheadVehicleSpeed.receive(fortySpeedMessage);
        aheadVehicleSpeed.receive(aheadVehicleAt60);

        ArgumentCaptor<Message> captor = ArgumentCaptor.forClass(Message.class);
        verify(bus).send(captor.capture());
        AheadVehicleSpeedMessage aheadVehicleSpeedMessage = (AheadVehicleSpeedMessage) captor.getValue();
        assertThat(aheadVehicleSpeedMessage.type(), is("AheadVehicleSpeed"));
        assertThat(aheadVehicleSpeedMessage.speed(), is(55f));
    }

    @Test
    public void send_a_message_with_the_speed_of_the_ahead_vehicle_two_times() {
        Bus bus = mock(Bus.class);
        AheadVehicleSpeed aheadVehicleSpeed = new AheadVehicleSpeed();
        aheadVehicleSpeed.setBus(bus);

        aheadVehicleSpeed.receive(fortySpeedMessage);
        aheadVehicleSpeed.receive(aheadDistanceVehicleAt30MessageInitial);

        aheadVehicleSpeed.receive(fortySpeedMessage);
        aheadVehicleSpeed.receive(aheadVehicleAt60);

        aheadVehicleSpeed.receive(fortySpeedMessage);
        aheadVehicleSpeed.receive(new AheadDistanceMessage() {
            @Override
            public int distance() {
                return 10;
            }

            @Override
            public String type() {
                return "AheadDistance";
            }

            @Override
            public LocalDateTime time() {
                return LocalDateTime.of(2016, 12, 20, 8, 0, 4);
            }
        });


        ArgumentCaptor<Message> captor = ArgumentCaptor.forClass(Message.class);
        verify(bus, times(2)).send(captor.capture());
        assertThat(((AheadVehicleSpeedMessage) captor.getAllValues().get(0)).speed(), is(55f));
        assertThat(((AheadVehicleSpeedMessage) captor.getAllValues().get(1)).speed(), is(15f));
    }
}
